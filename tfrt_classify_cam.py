import os
import copy
import time
import cv2
import numpy as np
import pycuda.autoinit
import pycuda.driver as cuda
import tensorrt as trt
from PIL import Image
import mediapipe as mp
from keras.applications.imagenet_utils import preprocess_input

class HostDeviceMem(object):
    def __init__(self, host_mem, device_mem):
        self.host = host_mem
        self.device = device_mem

    def __str__(self):
        return "Host:\n" + str(self.host) + "\nDevice:\n" + str(self.device)

    def __repr__(self):
        return self.__str__()

class Load_engine_model:
    def load_engine(self,trt_runtime, engine_path):
        with open(engine_path, "rb") as f:
            engine_data = f.read()
        engine = trt_runtime.deserialize_cuda_engine(engine_data)
        return engine
        
    def allocate_buffers(self,engine):
        # Determine dimensions and create page-locked memory buffers (i.e. won't be swapped to disk) to hold host inputs/outputs.
        h_input = cuda.pagelocked_empty(trt.volume(engine.get_binding_shape(0)), dtype=trt.nptype(trt.float32))
        h_output = cuda.pagelocked_empty(trt.volume(engine.get_binding_shape(1)), dtype=trt.nptype(trt.float32))
        # Allocate device memory for inputs and outputs.
        d_input = cuda.mem_alloc(h_input.nbytes)
        d_output = cuda.mem_alloc(h_output.nbytes)
        # Create a stream in which to copy inputs/outputs and run inference.
        stream = cuda.Stream()
        return h_input, d_input, h_output, d_output, stream

class Clasify_image_test:
    def load_normalized_test_case(self,test_image, pagelocked_buffer):
        # Converts the input image to a CHW Numpy array
        def normalize_image(image):
            # Resize, antialias and transpose the image to CHW.
            c, h, w = 3,224,224
            return preprocess_input(np.asarray(image.resize((w, h), Image.ANTIALIAS)).transpose([2, 0, 1]).astype(trt.nptype(trt.float32)), mode='caffe', data_format='channels_first').ravel()

        # Normalize the image and copy to pagelocked memory.
        np.copyto(pagelocked_buffer, normalize_image(Image.fromarray(test_image)))
        return test_image

    def do_inference(self,context, h_input, d_input, h_output, d_output, stream):
        # Transfer input data to the GPU.
        cuda.memcpy_htod_async(d_input, h_input, stream)
        # Run inference.
        context.execute_async(bindings=[int(d_input), int(d_output)], stream_handle=stream.handle)
        # Transfer predictions back from the GPU.
        cuda.memcpy_dtoh_async(h_output, d_output, stream)
        # Synchronize the stream
        stream.synchronize()
        return h_output,h_input
#object class
load_engine_object = Load_engine_model()
classify_image_object= Clasify_image_test()

def run_face_detection(
        face_detection,
        image,
        expansion_rate=[0.06, 0.36, 0.1, 0.0],  # x1, y1, x2, y2
):
    # 前処理:BGR->RGB
    input_image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)

    # 推論
    results = face_detection.process(input_image)

    # 後処理
    image_width, image_height = image.shape[1], image.shape[0]
    bboxes = []
    keypoints = []
    scores = []
    if results.detections is not None:
        for detection in results.detections:
            # バウンディングボックス
            bbox = detection.location_data.relative_bounding_box
            x1 = int(bbox.xmin * image_width)
            y1 = int(bbox.ymin * image_height)
            w = int(bbox.width * image_width)
            h = int(bbox.height * image_height)
            x1 = x1 - int(w * expansion_rate[0])
            y1 = y1 - int(h * expansion_rate[1])
            x2 = x1 + w + int(w * expansion_rate[0]) + int(
                w * expansion_rate[2])
            y2 = y1 + h + int(h * expansion_rate[1]) + int(
                h * expansion_rate[3])

            x1 = np.clip(x1, 0, image_width)
            y1 = np.clip(y1, 0, image_height)
            x2 = np.clip(x2, 0, image_width)
            y2 = np.clip(y2, 0, image_height)

            bboxes.append([x1, y1, x2, y2])

    return bboxes

def draw_detection(
    image,
    bboxes,
    as_results,
 
):
    for bbox, pred in zip(bboxes, as_results):
        
        if pred == "real":
            cv2.rectangle(image, (bbox[0], bbox[1]), (bbox[2], bbox[3]),
                         (255, 0, 0), 2)

            cv2.putText(image, str("real"),
                    (bbox[0], bbox[1] - 10), cv2.FONT_HERSHEY_SIMPLEX, 0.7,
                    (255, 0, 0), 1, cv2.LINE_AA)

        elif pred == "spoof":
            cv2.rectangle(image, (bbox[0], bbox[1]), (bbox[2], bbox[3]),
                         (0, 0, 255), 2)

            cv2.putText(image, str("spoof"),
                       (bbox[0], bbox[1] - 10), cv2.FONT_HERSHEY_SIMPLEX, 0.7,
                       (0, 0, 255), 1, cv2.LINE_AA)

    # cv.putText(image, "FPS:" + str(display_fps), (10, 30),
    #            cv.FONT_HERSHEY_SIMPLEX, 1.0, (0, 255, 0), 1, cv.LINE_AA)
    return image


def main():    
    
    # TensorRT logger singleton
    TRT_LOGGER = trt.Logger(trt.Logger.WARNING)
    trt_engine_path = os.path.join("FaceDetection-Anti-Spoof-Demo/export/final_model_int8.engine")
    
    trt_runtime = trt.Runtime(TRT_LOGGER)
    trt_engine = load_engine_object.load_engine(trt_runtime, trt_engine_path)
    
    # This allocates memory for network inputs/outputs on both CPU and GPU
    h_input, d_input, h_output, d_output, stream = load_engine_object.allocate_buffers(trt_engine)
    
    # Execution context is needed for inference
    context = trt_engine.create_execution_context()

    # -------------- MODEL PARAMETERS FOR THE MODEL --------------------------------
    model_h = 224
    model_w = 224
    labels_file = "FaceDetection-Anti-Spoof-Demo/labels.txt"
    labels = open(labels_file, 'r').read().split('\n')

    mp_face_detection = mp.solutions.face_detection
    face_detection = mp_face_detection.FaceDetection(
        model_selection=0,
        min_detection_confidence=0.6,
    )
    cap = cv2.VideoCapture(0)

    while True:
        #loop over the images
        ret, frame = cap.read()
        if not ret:
            break
        frame_cp = copy.deepcopy(frame)

        bboxes = run_face_detection(face_detection, frame)

        as_results=[]
       
        for bbox in bboxes:
            face_crop = frame[bbox[1]:bbox[3], bbox[0]:bbox[2]]
            face_crop = cv2.cvtColor(face_crop, cv2.COLOR_BGR2RGB)
        # test_image = cv2.imread(image_name)
            test_case = classify_image_object.load_normalized_test_case(face_crop, h_input)

            start_time = time.time()
            h_output,h_input = classify_image_object.do_inference(context, h_input, d_input, h_output, d_output, stream)
            pred = labels[np.argmax(h_output)]
            as_results.append(pred)
        
        
        frame_cp=draw_detection(frame_cp, bboxes, as_results)

        key=cv2.waitKey(0)
        if key == 27:
            break
        cv2.imshow("SHOW", frame_cp)
    
    cap.release()
    cv2.destroyAllWindows()



if __name__ =='__main__':
    main()
