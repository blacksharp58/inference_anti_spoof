import copy
import argparse
import os
import cv2 as cv
import time
import numpy as np
# import mediapipe as mp
import onnxruntime
import tensorflow as tf
from utils.cvfpscalc import CvFpsCalc
from PIL import Image
import pandas as pd

SAVE_IMAGE = '/home/fuonq/Code/anti-spoof/data_test/spoof_save/'

PATH_IMAGE = "/home/fuonq/Code/anti-spoof/data_test/spoof_crop/"

def get_args():
    parser = argparse.ArgumentParser()

    parser.add_argument("--fd_model_selection", type=int, default=0)
    parser.add_argument(
        "--min_detection_confidence",
        help='min_detection_confidence',
        type=float,
        default=0.7,
    )
    parser.add_argument("--min_spoofing_confidence", 
                        type=float,
                        default=0.4, )
    parser.add_argument(
        "--as_model",
        type=str,
        default='FaceDetection-Anti-Spoof-Demo/anti-spoof-mn3/mobilenet_v2_100_224.tflite',
    )
    parser.add_argument(
        "--as_input_size",
        type=str,
        default='224,224',
    )

    args = parser.parse_args()

    return args


def draw_detection(
    image_name,
    image,
    bboxes,
    scores,
    as_results,
):  
    count0=0
    count1=0
    for bbox, as_result in zip(bboxes, as_results):
        # bounding box
        as_index = np.argmax(as_result)
        # 0: real - 1: fake
        if as_index == 0:
            cv.rectangle(image, (bbox[0], bbox[1]), (bbox[2], bbox[3]),
                         (255, 0, 0), 2)

            # cv.putText(image, str("real-") + str(round(as_result[as_index], 3)),
            #            (bbox[0], bbox[1] - 10), cv.FONT_HERSHEY_SIMPLEX, 0.7,
            #            (255, 0, 0), 1, cv.LINE_AA)

            # count0 +=1    
            # image_cut = image[bbox[1]:bbox[3], bbox[0]:bbox[2]]
            # format_ = os.path.splitext(image_name)[-1]
            # result_image_name = image_name.replace(format_, "_result" + format_)
            # cv.imwrite(SAVE_IMAGE + result_image_name, image_cut)       

        elif as_index == 1:
            cv.rectangle(image, (bbox[0], bbox[1]), (bbox[2], bbox[3]),
                         (0, 0, 255), 2)
            
        #     cv.putText(image, str("fake-") + str(round(as_result[as_index], 3)),
        #                (bbox[0], bbox[1] - 10),  cv.FONT_HERSHEY_SIMPLEX, 0.7,
        #                (0, 0, 255), 1,  cv.LINE_AA)                
            count1 +=1
            # print(count1)

            format_ = os.path.splitext(image_name)[-1]
            # image_cut = image[bbox[1]:bbox[3], bbox[0]:bbox[2]]
            # result_image_name = image_name.replace(format_, "_result" + format_)
            # cv.imwrite(SAVE_IMAGE + result_image_name, image_cut)

    return image,count0,count1



def run_anti_spoof(frame, input_height=224, input_width=224, input_mean=0, input_std=255):
    # 前処理:リサイズ, BGR->RGB, 標準化, 成形, float32キャスト
    # img_rgb = cv.cvtColor(frame, cv.COLOR_BGR2RGB)
    # output_name = "normailized"
    # float_caster = tf.cast(img_rgb, tf.float32)
    # dims_expander = tf.expand_dims(float_caster, 0)
    # resized = tf.image.resize_bilinear(dims_expander, [input_height, input_width])
    # normalized = tf.divide(tf.subtract(resized,[input_mean]), [input_std])
    # sess = tf.Session()
    # result = sess.run(normalized)

    # return result
    img = cv2.resize(img, (input_width, input_height))
        # img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
    input_data = np.expand_dims(img, axis=0).astype('float32')
    return input_data



def main():

    args = get_args()


    ##Load tflite model and allocate tensors
    as_model_path = args.as_model
    interpreter = tf.lite.Interpreter(model_path=as_model_path)
    interpreter.allocate_tensors()
    # Get input and output tensors.
    input_details = interpreter.get_input_details()
    output_details = interpreter.get_output_details()

    
    for i, img in enumerate(os.listdir(PATH_IMAGE)):

        test_speed = 0
        image=cv.imread(PATH_IMAGE + img)

        # fd_model_selection = args.fd_model_selection
        # min_detection_confidence = args.min_detection_confidence
        # as_input_size = [int(i) for i in args.as_input_size.split(',')]


        # bboxes, scores = run_face_detection(face_detection, image)

  
        # spoofing detection
        start_time=time.time()
  
        image_tensor = run_anti_spoof(image,224, 224)
            ##Test model
        interpreter.set_tensor(input_details[0]['index'], image_tensor)
        interpreter.invoke()
        output_data = interpreter.get_tensor(output_details[0]['index']) 
        
        print("Name= {} _ Output_score: {}".format(img,as_results))
        test_speed+=time.time() - start_time
        print("test_speed: {:.2f} s".format(test_speed))

        



if __name__ == '__main__':

    main()
    