import copy
import argparse

import cv2 as cv
import numpy as np
import mediapipe as mp

from PIL import Image
import tensorflow as tf

from utils import CvFpsCalc


def get_args():
    parser = argparse.ArgumentParser()

    parser.add_argument("--device", type=int, default=0)
    parser.add_argument("--width", help='cap width', type=int, default=1280)
    parser.add_argument("--height", help='cap height', type=int, default=720)

    parser.add_argument("--fd_model_selection", type=int, default=0)
    parser.add_argument(
        "--min_detection_confidence",
        help='min_detection_confidence',
        type=float,
        default=0.7,
    )

    parser.add_argument(
        "--as_model",
        type=str,
        default='anti-spoof-mn3/mobilenet_v2_100_224.tflite',
    )
    parser.add_argument(
        "--as_input_size",
        type=str,
        default='224,224',
    )

    args = parser.parse_args()

    return args


def run_face_detection(
        face_detection,
        image,
        expansion_rate=[0.1, 0.4, 0.1, 0.0],  # x1, y1, x2, y2
):
    # 前処理:BGR->RGB
    input_image = cv.cvtColor(image, cv.COLOR_BGR2RGB)

    # 推論
    results = face_detection.process(input_image)

    # 後処理
    image_width, image_height = image.shape[1], image.shape[0]
    bboxes = []
    keypoints = []
    scores = []
    if results.detections is not None:
        for detection in results.detections:
            # バウンディングボックス
            bbox = detection.location_data.relative_bounding_box
            x1 = int(bbox.xmin * image_width)
            y1 = int(bbox.ymin * image_height)
            w = int(bbox.width * image_width)
            h = int(bbox.height * image_height)
            x1 = x1 - int(w * expansion_rate[0])
            y1 = y1 - int(h * expansion_rate[1])
            x2 = x1 + w + int(w * expansion_rate[0]) + int(
                w * expansion_rate[2])
            y2 = y1 + h + int(h * expansion_rate[1]) + int(
                h * expansion_rate[3])

            x1 = np.clip(x1, 0, image_width)
            y1 = np.clip(y1, 0, image_height)
            x2 = np.clip(x2, 0, image_width)
            y2 = np.clip(y2, 0, image_height)

            bboxes.append([x1, y1, x2, y2])

            # スコア
            scores.append(detection.score[0])
    return bboxes, scores


def run_anti_spoof(frame, input_height=224, input_width=224, input_mean=0, input_std=225):
    # 前処理:リサイズ, BGR->RGB, 標準化, 成形, float32キャスト
    img_rgb = cv.cvtColor(frame, cv.COLOR_BGR2RGB)
    output_name = "normailized"
    float_caster = tf.cast(img_rgb, tf.float32)
    dims_expander = tf.expand_dims(float_caster, 0)
    resized = tf.image.resize_bilinear(dims_expander, [input_height, input_width])
    normalized = tf.divide(tf.subtract(resized,[input_mean]), [input_std])
    sess = tf.Session()
    result = sess.run(normalized)

    return result


def main():
    # 引数解析 #################################################################
    args = get_args()
    label=["live", "spoof"]
    cap_device = args.device
    cap_width = args.width
    cap_height = args.height

    fd_model_selection = args.fd_model_selection
    min_detection_confidence = args.min_detection_confidence

    as_model_path = args.as_model
    as_input_size = [int(i) for i in args.as_input_size.split(',')]

    # カメラ準備 ###############################################################
    cap = cv.VideoCapture(cap_device)
    cap.set(cv.CAP_PROP_FRAME_WIDTH, cap_width)
    cap.set(cv.CAP_PROP_FRAME_HEIGHT, cap_height)

    # モデルロード #############################################################
    # 顔検出
    mp_face_detection = mp.solutions.face_detection
    face_detection = mp_face_detection.FaceDetection(
        model_selection=fd_model_selection,
        min_detection_confidence=min_detection_confidence,
    )

    interpreter = tf.lite.Interpreter(model_path=as_model_path)
    interpreter.allocate_tensors()
    # Get input and output tensors.
    input_details = interpreter.get_input_details()
    output_details = interpreter.get_output_details()

    # FPS計測モジュール ########################################################
    cvFpsCalc = CvFpsCalc(buffer_len=10)

    while True:
        display_fps = cvFpsCalc.get()

        # カメラキャプチャ #####################################################
        ret, image = cap.read()
        if not ret:
            break
        debug_image = copy.deepcopy(image)

        # 検出実施 #############################################################
        # 顔検出
        bboxes, scores = run_face_detection(face_detection, image)

        # なりすまし検出
        as_results = []
        for bbox in bboxes:
            face_image = image[bbox[1]:bbox[3], bbox[0]:bbox[2]]
            image_tensor = run_anti_spoof(face_image,224,224)
            interpreter.set_tensor(input_details[0]['index'], image_tensor)
            interpreter.invoke()
            output_data = interpreter.get_tensor(output_details[0]['index']) 
            as_results.append(output_data)

        # 描画 #################################################################
        debug_image = draw_detection(
            debug_image,
            bboxes,
            scores,
            as_results,
            # display_fps,
        )

        # キー処理(ESC：終了) ##################################################
        key = cv.waitKey(1)
        if key == 27:  # ESC
            break

        # 画面反映 #############################################################
        cv.imshow('Anti Sppof Demo', debug_image)

    cap.release()
    cv.destroyAllWindows()


def draw_detection(
    image,
    bboxes,
    scores,
    as_results,
    # display_fps,
):
    for bbox, as_result in zip(bboxes, as_results):
        # バウンディングボックス
        as_index = np.argmax(as_result)
        
        if as_index == 0:
            cv.rectangle(image, (bbox[0], bbox[1]), (bbox[2], bbox[3]),
                         (255, 0, 0), 2)
            
            cv.putText(image, str("real"),
            (bbox[0], bbox[1] - 10), cv.FONT_HERSHEY_SIMPLEX, 0.7,
            (255, 0, 0), 1, cv.LINE_AA)

        elif as_index == 1:
            cv.rectangle(image, (bbox[0], bbox[1]), (bbox[2], bbox[3]),
                         (0, 0, 255), 2)
            cv.putText(image, str("spoof"),
            (bbox[0], bbox[1] - 10), cv.FONT_HERSHEY_SIMPLEX, 0.7,
            (0, 0, 255), 1, cv.LINE_AA)


    # cv.putText(image, "FPS:" + str(display_fps), (10, 30),
    #            cv.FONT_HERSHEY_SIMPLEX, 1.0, (0, 255, 0), 1, cv.LINE_AA)
    return image


if __name__ == '__main__':
    main()
