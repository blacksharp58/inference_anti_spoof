import os
import time
import cv2
import numpy as np
import pycuda.autoinit
import pycuda.driver as cuda
import tensorrt as trt
from PIL import Image
from keras.applications.imagenet_utils import preprocess_input

class HostDeviceMem(object):
    def __init__(self, host_mem, device_mem):
        self.host = host_mem
        self.device = device_mem

    def __str__(self):
        return "Host:\n" + str(self.host) + "\nDevice:\n" + str(self.device)

    def __repr__(self):
        return self.__str__()

class Load_engine_model:
    def load_engine(self,trt_runtime, engine_path):
        with open(engine_path, "rb") as f:
            engine_data = f.read()
        engine = trt_runtime.deserialize_cuda_engine(engine_data)
        return engine
        
    def allocate_buffers(self,engine):
        # Determine dimensions and create page-locked memory buffers (i.e. won't be swapped to disk) to hold host inputs/outputs.
        h_input = cuda.pagelocked_empty(trt.volume(engine.get_binding_shape(0)), dtype=trt.nptype(trt.float32))
        h_output = cuda.pagelocked_empty(trt.volume(engine.get_binding_shape(1)), dtype=trt.nptype(trt.float32))
        # Allocate device memory for inputs and outputs.
        d_input = cuda.mem_alloc(h_input.nbytes)
        d_output = cuda.mem_alloc(h_output.nbytes)
        # Create a stream in which to copy inputs/outputs and run inference.
        stream = cuda.Stream()
        return h_input, d_input, h_output, d_output, stream

class Clasify_image_test:
    def load_normalized_test_case(self,test_image, pagelocked_buffer):
        # Converts the input image to a CHW Numpy array
        def normalize_image(image):
            # Resize, antialias and transpose the image to CHW.
            c, h, w = 3,224,224
            return preprocess_input(np.asarray(image.resize((w, h), Image.ANTIALIAS)).transpose([2, 0, 1]).astype(trt.nptype(trt.float32)), mode='caffe', data_format='channels_first').ravel()

        # Normalize the image and copy to pagelocked memory.
        np.copyto(pagelocked_buffer, normalize_image(Image.open(test_image)))
        return test_image

    def do_inference(self,context, h_input, d_input, h_output, d_output, stream):
        # Transfer input data to the GPU.
        cuda.memcpy_htod_async(d_input, h_input, stream)
        # Run inference.
        context.execute_async(bindings=[int(d_input), int(d_output)], stream_handle=stream.handle)
        # Transfer predictions back from the GPU.
        cuda.memcpy_dtoh_async(h_output, d_output, stream)
        # Synchronize the stream
        stream.synchronize()
        return h_output,h_input
#object class
load_engine_object = Load_engine_model()
classify_image_object= Clasify_image_test()

def main():    
    count = 0
    # TensorRT logger singleton
    TRT_LOGGER = trt.Logger(trt.Logger.WARNING)
    trt_engine_path = os.path.join("FaceDetection-Anti-Spoof-Demo/export/final_model_int8.engine")
    
    trt_runtime = trt.Runtime(TRT_LOGGER)
    trt_engine = load_engine_object.load_engine(trt_runtime, trt_engine_path)
    
    # This allocates memory for network inputs/outputs on both CPU and GPU
    h_input, d_input, h_output, d_output, stream = load_engine_object.allocate_buffers(trt_engine)
    
    # Execution context is needed for inference
    context = trt_engine.create_execution_context()

    # -------------- MODEL PARAMETERS FOR THE MODEL --------------------------------
    model_h = 224
    model_w = 224
    img_dir = "data_test/spoof_crop"
    
    count_real = 0
    count_spoof =0
    for image in os.listdir(img_dir):
        #loop over the images
        image_name = img_dir + "/" + image
        labels_file = "FaceDetection-Anti-Spoof-Demo/labels.txt"
        labels = open(labels_file, 'r').read().split('\n')

        # test_image = cv2.imread(image_name)
        test_case = classify_image_object.load_normalized_test_case(image_name, h_input)

        start_time = time.time()
        h_output,h_input = classify_image_object.do_inference(context, h_input, d_input, h_output, d_output, stream)
        pred = labels[np.argmax(h_output)]

        #print (test_image)

        if pred=="real":
            count_real+=1
        elif pred=="spoof":
            count_spoof+=1
        # print (image,"class: ",pred,", Accuracy: ", max(h_output))
        # print ("Inference Time : ",time.time()-start_time)
    print("Real == {} ___ Spoof == {}".format(count_real, count_spoof))

if __name__ =='__main__':
    main()
