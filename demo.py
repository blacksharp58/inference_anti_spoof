import copy
import argparse
import os
import cv2 as cv
import time
import numpy as np
import mediapipe as mp
import onnxruntime

from utils.cvfpscalc import CvFpsCalc

SAVE_IMAGE = '/home/fuonq/Code/anti-spoof/data_test/spoof_save/'
PATH_IMAGE = "/home/fuonq/Code/anti-spoof/FaceDetection-Anti-Spoof-Demo/LCC_FASD/LCC_FASD_evaluation/spoof/"

def get_args():
    parser = argparse.ArgumentParser()

    parser.add_argument("--fd_model_selection", type=int, default=0)
    parser.add_argument(
        "--min_detection_confidence",
        help='min_detection_confidence',
        type=float,
        default=0.7,
    )
    parser.add_argument("--min_spoofing_confidence", 
                        type=float,
                        default=0.4, )
    parser.add_argument(
        "--as_model",
        type=str,
        default='FaceDetection-Anti-Spoof-Demo/anti-spoof-mn3/mobilenetv2_default.onnx',
    )
    parser.add_argument(
        "--as_input_size",
        type=str,
        default='128,128',
    )

    args = parser.parse_args()

    return args


def run_face_detection(
        face_detection,
        image,
        expansion_rate=[0.1, 0.45, 0.1, 0.0],  # x1, y1, x2, y2
):
    # preprocessing:BGR->RGB
    input_image = cv.cvtColor(image, cv.COLOR_BGR2RGB)

    # inference
    results = face_detection.process(input_image)

    # post-processing
    image_width, image_height = image.shape[1], image.shape[0]
    bboxes = []
    keypoints = []
    scores = []
    if results.detections is not None:
        for detection in results.detections:
            # bounding box
            bbox = detection.location_data.relative_bounding_box
            x1 = int(bbox.xmin * image_width)
            y1 = int(bbox.ymin * image_height)
            w = int(bbox.width * image_width)
            h = int(bbox.height * image_height)
            x1 = x1 - int(w * expansion_rate[0])
            y1 = y1 - int(h * expansion_rate[1])
            x2 = x1 + w + int(w * expansion_rate[0]) + int(
                w * expansion_rate[2])
            y2 = y1 + h + int(h * expansion_rate[1]) + int(
                h * expansion_rate[3])

            x1 = np.clip(x1, 0, image_width)
            y1 = np.clip(y1, 0, image_height)
            x2 = np.clip(x2, 0, image_width)
            y2 = np.clip(y2, 0, image_height)

            bboxes.append([x1, y1, x2, y2])

            # score
            scores.append(detection.score[0])
    return bboxes,  scores

def draw_detection(
    image_name,
    image,
    bboxes,
    scores,
    as_results,
):  
    count0=0
    count1=0
    for bbox, as_result in zip(bboxes, as_results):
        # bounding box
        as_index = np.argmax(as_result)
        # 0: real - 1: fake
        if as_index == 0:
            # cv.rectangle(image, (bbox[0], bbox[1]), (bbox[2], bbox[3]),
            #              (255, 0, 0), 2)

            # cv.putText(image, str("real-") + str(round(as_result[as_index], 3)),
            #            (bbox[0], bbox[1] - 10), cv.FONT_HERSHEY_SIMPLEX, 0.7,
            #            (255, 0, 0), 1, cv.LINE_AA)

            count0 +=1    
            # image_cut = image[bbox[1]:bbox[3], bbox[0]:bbox[2]]
            # format_ = os.path.splitext(image_name)[-1]
            # result_image_name = image_name.replace(format_, "_result" + format_)
            # cv.imwrite(SAVE_IMAGE + result_image_name, image_cut)       

        elif as_index == 1:
        #     cv.rectangle(image, (bbox[0], bbox[1]), (bbox[2], bbox[3]),
        #                  (0, 0, 255), 2)
            
        #     cv.putText(image, str("fake-") + str(round(as_result[as_index], 3)),
        #                (bbox[0], bbox[1] - 10),  cv.FONT_HERSHEY_SIMPLEX, 0.7,
        #                (0, 0, 255), 1,  cv.LINE_AA)                
            count1 +=1
            # print(count1)

            # format_ = os.path.splitext(image_name)[-1]
            # image_cut = image[bbox[1]:bbox[3], bbox[0]:bbox[2]]
            # result_image_name = image_name.replace(format_, "_result" + format_)
            # cv.imwrite(SAVE_IMAGE + result_image_name, image_cut)

    return image,count0,count1


def run_anti_spoof(onnx_session, input_size, image):
    # Pretreatment: Resize, BGR-> RGB, Standardization, Molding, float32 Cast
    input_image = cv.resize(image, dsize=(input_size[1], input_size[0]))
    input_image = cv.cvtColor(input_image, cv.COLOR_BGR2RGB)

    input_image = input_image.transpose(2, 0, 1).astype('float32')
    input_image = input_image.reshape(-1, 3, input_size[1], input_size[0])

    # inference
    input_name = onnx_session.get_inputs()[0].name
    result = onnx_session.run(None, {input_name: input_image})

    # post=processing
    result = np.array(result)
    result = np.squeeze(result)

    return result


def main():

    args = get_args()
    count_real = 0
    count_fake = 0
    for i, img in enumerate(os.listdir(PATH_IMAGE)):

        test_speed = 0
        image=cv.imread(PATH_IMAGE + img)

        fd_model_selection = args.fd_model_selection
        min_detection_confidence = args.min_detection_confidence

        as_model_path = args.as_model
        as_input_size = [int(i) for i in args.as_input_size.split(',')]

        # face-detection
        mp_face_detection = mp.solutions.face_detection
        face_detection = mp_face_detection.FaceDetection(
            model_selection=fd_model_selection,
            min_detection_confidence=min_detection_confidence,
        )

        # spoofing detection
        # onnx_session = onnxruntime.InferenceSession(as_model_path)

        debug_image = copy.deepcopy(image)

        # detection implementation #############################################################
        # face-detection
        start_time = time.time()

        bboxes, scores = run_face_detection(face_detection, image)
        if not len(bboxes):
            os.remove(PATH_IMAGE + img)
        # spoofing detection
    #     as_results = []
    #     for bbox in bboxes:
    #         face_image = image[bbox[1]:bbox[3], bbox[0]:bbox[2]]
    #         as_result = run_anti_spoof(onnx_session, as_input_size, face_image)
    #         as_results.append(as_result)
    #     # print(as_results)

    #     test_speed+=time.time() - start_time
    #     print("test_speed: {:.2f} s".format(test_speed))
    #     # drawing #################################################################
    #     debug_image,flag_real, flag_fake = draw_detection(
    #         img,
    #         debug_image,
    #         bboxes,
    #         scores,
    #         as_results
    #     )


    #     if flag_fake==1:
    #         count_fake+=1
    #     if flag_real==1:
    #         count_real+=1


    #     # cv.imshow("test", debug_image)
    #     # cv.waitKey(0)
    # print("Real face: ", count_real)
    # print("Fake face: ", count_fake)





if __name__ == '__main__':

    main()
    